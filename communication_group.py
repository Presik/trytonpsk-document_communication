# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction


class CommunicationGroup(ModelSQL, ModelView):
    'Communication Group'
    __name__ = 'document.communication.group'
    name = fields.Char('Name', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    users = fields.Many2Many('res.user-communication.group',
        'group', 'user', 'Users')

    @classmethod
    def __setup__(cls):
        super(CommunicationGroup, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False


class CommunicationGroupUser(ModelSQL):
    'Communication Group User'
    __name__ = 'res.user-communication.group'
    _table = 'res_user_comunication_department'
    group = fields.Many2One('document.communication.group', 'Group',
        ondelete='CASCADE', select=True,  required=True)
    user = fields.Many2One('res.user', 'User', select=True,
        required=True, ondelete='RESTRICT')
