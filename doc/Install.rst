Install Guide Document Module
#############################

Steps for install and config the document management module.


1. Install ftp server

	$ sudo apt-get install vsftpd


2. Edit ftp server configuration file:

	$ nano /etc/vsftpd.conf

	Note: 1. See [1]

	      2. Uncomment and set on config file this line:

		pam_service_name=ftp


3. Create ftp users list file:

	$ sudo echo 'ftpuser' >> /etc/vsftpd.chroot_list


4. To create ftp user 

	$ sudo useradd -m ftpuser
	$ sudo passwd ftpuser


5. Add ftpuser to ftp group:

	sudo addgroup ftpuser ftp


6. Restart ftp server:

	$ sudo systemctl restart vsftpd


7. Add to trytond.conf file next lines:

	[path_dms]
	# Configure the path to store attachments on Document Management System
	path_home=/home/ftpuser/files
	path_localhost=ftp://ftpuser@XX.XX.XX.XX/files
	path_ftp=ftp://ftpuser:mypassword@XX.XX.XX.XX:port/files
	dir_comm=comunicaciones


8. Go to Tryton module Document Management and create name company base for path the files:

	|_  Document Management
 		|_ Configuration
			|_ Configuration
				
	Set Company name in company field (Without blank spaces).


[1] https://wiki.archlinux.org/index.php/Very_Secure_FTP_Daemon

