# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import communication
from . import configuration
from . import communication_group


def register():
    Pool.register(
        communication_group.CommunicationGroup,
        communication_group.CommunicationGroupUser,
        communication.LetterReference,
        communication.CommunicationKind,
        communication.Communication,
        communication.CommunicationLetter,
        communication.CommunicationAttachment,
        configuration.Configuration,
        communication.DocumentCommunicationUser,
        communication.CommunicationFixNumberStart,
        communication.CommunicationAddUserStart,
        communication.CommunicationSummaryStart,
        communication.AddAttachmentStart,
        module='document_communication', type_='model')
    Pool.register(
        communication.CommunicationLetterReport,
        communication.TagReport,
        communication.CommunicationSummaryReport,
        module='document_communication', type_='report')
    Pool.register(
        communication.AddAttachment,
        communication.CommunicationFixNumber,
        communication.CommunicationAddUser,
        communication.CommunicationSummary,
        communication.CommunicationForceDraft,
        communication.CommunicationLetterForceDraft,
        module='document_communication', type_='wizard')
